//
//  ApiRequest.m
//  testAppAsa
//
//  Created by Alexey on 02.05.15.
//  Copyright (c) 2015 Alexey. All rights reserved.
//

#import "ApiRequest.h"
#import "NapClient.h"
#import "SimpleKeyChain.h"

#define SERVER_IP @"http://_____________"
#define _clientsUrl @"GetAccountClientList.ashx?login=%@&password=%@&token=%@"
#define _isReadRequest @"SetReadChangesClientFromAgent.ashx?login=%@&password=%@&token=%@&clientid=%i"
#define _keychain_login_key @"_in_keychain_login"
#define _keychain_password_key @"_in_keychain_password"

// метод конкатенации сервера и его метода для запроса
NS_INLINE NSString* NapMakeApiUrl(NSString *method) {
    return [NSString stringWithFormat:@"%@%@", SERVER_IP, method];
}


@implementation ApiRequest

- (BOOL)isNetworkConnected {
    return [reachability isReachable];
}

- (void)getAllClientsWithCompletition:(void (^)(NSDictionary *, NSError *))clientsBlock {
    if ([self isNetworkConnected]) {
        NSString *urlStr = [NSString stringWithFormat:NapMakeApiUrl(_clientsUrl),
                            [SimpleKeyChain loadValueForKey:_keychain_login_key],
                            [SimpleKeyChain loadValueForKey:_keychain_password_key],
                            @"12345"];
        
        NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlStr]
                                                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                     
                                                                     if (data && response) {
                                                                         NSError *err;
                                                                         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
                                                                         if (err) { clientsBlock(nil, err); return; }
                                                                         if ([json[@"error"] boolValue]) {
                                                                             clientsBlock(nil, [NSError errorWithDomain:@"parseClientsError" code:1 userInfo:@{NSLocalizedDescriptionKey : json[@"errorText"]}]);
                                                                             return;
                                                                         }
                                                                         if (!json[@"result"] || ![json[@"result"] count]) {
                                                                             clientsBlock(nil, [NSError errorWithDomain:@"parseClientsError" code:2 userInfo:@{NSLocalizedDescriptionKey : @"no result"}]);
                                                                             return;
                                                                         }
                                                                         [self parseClientsWithArray:json[@"result"]];
                                                                         clientsBlock([self createAlphabetDictionaryWithFilter:0], nil);
                                                                     } else {
                                                                         clientsBlock(nil, error);
                                                                     }
                                                                 }];
        [task resume];
    } else
        clientsBlock([self createAlphabetDictionaryWithFilter:0], nil);
}


- (NSMutableDictionary *)createAlphabetDictionaryWithFilter:(NSInteger)filterId {
    NSString *filterPredicateSt = [self filterPredicateWithId:filterId];
    
    NSPredicate *predicateReadYes = (BOOL)filterPredicateSt ? [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isRead == YES AND isSelected = YES AND %@", filterPredicateSt]] : [NSPredicate predicateWithFormat:@"isRead == YES"];
    NSPredicate *predicateReadNo = (BOOL)filterPredicateSt ? [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isRead == NO OR isSelected = NO AND %@", filterPredicateSt]] : [NSPredicate predicateWithFormat:@"isRead == NO OR isSelected = NO"];
    
    
    NSArray*isReadArray = [NapClient MR_findAllSortedBy:@"clientId" ascending:NO
                                          withPredicate:predicateReadYes
                                              inContext:[NSManagedObjectContext MR_defaultContext]];
    
    NSArray*isNoReadArray = [NapClient MR_findAllSortedBy:@"clientId" ascending:NO
                                            withPredicate:predicateReadNo
                                                inContext:[NSManagedObjectContext MR_defaultContext]];
    
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];
    mutableDictionary[@"*"] = isNoReadArray;
    [mutableDictionary addEntriesFromDictionary:[self composeUsersArray:isReadArray]];
    return mutableDictionary;
}

-(NSDictionary *) composeUsersArray:(NSArray *) clients{
    NSMutableDictionary *usersDictionary = [NSMutableDictionary new];
    
    for(NapClient *user in clients){
        NSMutableArray *letterList;
        NSString *firstLetter = @"#";
        
        if (!user.firstName){
            letterList = usersDictionary[firstLetter];
        } else{
            NSString *section = [[user.firstName substringToIndex:1] uppercaseString];
            unichar c = [section characterAtIndex:0];
            if ([section hasPrefix:@"+"] || (c >= '0' && c <= '9'))
                section = @"#";
            firstLetter = section;
            letterList = usersDictionary[firstLetter];
        }
        
        if (!letterList){
            letterList = [NSMutableArray array];
            usersDictionary[firstLetter] = letterList;
        }
        
        [letterList addObject:user];
    }
    
    return usersDictionary;
}

- (void) parseClientsWithArray:(NSArray *) array {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *cDict in array) {
            NapClient *client = [NapClient MR_findFirstByAttribute:@"clientId" withValue:cDict[@"clientid"] inContext:localContext];
            
            if (!client) {
                client = [NapClient MR_createEntityInContext:localContext];
                client.clientId = cDict[@"clientid"];
            }
            client.accountId = cDict[@"accountclientid"];
            client.firstName = cDict[@"firstname"];
            client.lastName = cDict[@"lastname"];
            client.patronymic = cDict[@"patronymic"];
            client.countryId = cDict[@"countryid"];
            client.countryName = cDict[@"countryname"];
            client.cityName = cDict[@"cityname"];
            client.cityId = cDict[@"cityid"];;
            client.phone = cDict[@"phone"];
            client.email = cDict[@"email"];
            client.serviceTypeId = cDict[@"servicetypeid"];
            client.isNewStatus = cDict[@"isnew"];
            client.isRead = cDict[@"isread"];
            //   client.isSelected = @NO;
            client.isPotentialClient = cDict[@"ispotential"];
            client.isActiveStatus = cDict[@"isactive"];
            client.currentStatus = cDict[@"currentstatus"];
            
            client.urlImage = cDict[@"imageurl"];
            
            
#if DEBUG == 1 || BETA == 1
            NSLog(@"image hash = %@", [client.image imageHashMD5]);
            NSLog(@"image hash = %@", cDict[@"imagehash"]);
#endif
            
            [client.socilaNets makeObjectsPerformSelector:@selector(MR_deleteEntityInContext:) withObject:localContext];
            
            for (NSDictionary *sDict in cDict[@"social"]) {
                NapSocialNet *social = [NapSocialNet MR_createEntityInContext:localContext];
                social.typeId = sDict[@"socialtypeid"];
                social.url = sDict[@"socialurl"];
                social.user = client;
            }
        }
    }];
}

-(NSString *) filterPredicateWithId:(NSInteger) filterId{
    NSString *filterPredicateSt = nil;
    switch (filterId) {
        case 1:
        case 2:  filterPredicateSt = [NSString stringWithFormat:@"isNewStatus = %ld", filterId%2]; break;
        case 3:
        case 4:  filterPredicateSt = [NSString stringWithFormat:@"isActiveStatus = %ld", filterId%2]; break;
        case 5:
        case 6:  filterPredicateSt = [NSString stringWithFormat:@"isPotentialClient = %ld", filterId%2]; break;
        default: break;
    }
    return filterPredicateSt;
}


@end
