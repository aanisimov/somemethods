//
//  NSMutableAttributedString+Translate.h
//  simpleTranslate
//
//  Created by Alexey on 26.02.15.
//  Copyright (c) 2015 vkrotin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString(Translate)

-(id)initWithTranslateDictionary:(NSArray *) trDictionary;

@end
