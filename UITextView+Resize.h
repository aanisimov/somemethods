//
//  UITextView+Resize.h
//  simpleTranslate
//
//  Created by Aleksey Anisimov on 26.02.15.
//  Copyright (c) 2015 vkrotin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITextView(Resize)

- (CGFloat)changeHeightWithHeight:(CGFloat)maxHeight;

- (void)scrollToBottom;
@end
