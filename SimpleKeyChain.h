//
//  SimpleKeyChain.h
//  Interview
//
//  Created by Alexey on 15.04.15.
//  Copyright (c) 2015 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleKeyChain : NSObject

+ (void) saveValue:(id)data forKey:(NSString *) key;
+ (id) loadValueForKey:(NSString *) key;
+ (void) deleteValueForKey:(NSString *) key;

@end
