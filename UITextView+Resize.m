//
//  UITextView+Resize.m
//  simpleTranslate
//
//  Created by Aleksey Anisimov on 26.02.15.
//  Copyright (c) 2015 vkrotin. All rights reserved.
//

#import "UITextView+Resize.h"

@implementation UITextView(Resize)

- (CGFloat)changeHeightWithHeight:(CGFloat)maxHeight {
    if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)])
    {
        CGRect frame = self.bounds;

        UIEdgeInsets textContainerInsets = self.textContainerInset;
        UIEdgeInsets contentInsets = self.contentInset;

        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + self.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;

        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;

        NSString *textToMeasure = self.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", self.text];
        }

        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];

        NSDictionary *attributes = @{ NSFontAttributeName: self.font, NSParagraphStyleAttributeName : paragraphStyle };

        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];

        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight <=maxHeight ? measuredHeight : maxHeight ;
    }
    else
    {
        return self.contentSize.height;
    }
}




-(void) scrollToBottom {
    CGRect caretRect = [self caretRectForPosition:self.endOfDocument];
    [self scrollRectToVisible:caretRect animated:NO];
}


@end
