//
//  ApiRequest.h
//  testAppAsa
//
//  Created by Alexey on 02.05.15.
//  Copyright (c) 2015 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiRequest : NSObject

/*!
 * @discussion Запрос для отправки запроса на сервер, для получения всех клиентов
 * @discussion сохранение модели данных в локальную базу
 * @param clientsBlock завершаюший блок с параметром ошибки, для проверки.
 */
- (void)getAllClientsWithCompletition:(void (^)(NSDictionary *, NSError *))clientsBlock;

@end
